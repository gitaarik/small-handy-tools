# Small handy tools

Some small handy one-page-html tools that help you out with small tasks.

### Notepad

For quick scribbles

### Codepad

For quick code scribbles

### Calculator

For quick simple calculations

### Notes

For saving temporary notes

### World clock

For checking the time, all over the world
