#!/usr/bin/env node
const fs = require('fs');
const minify = require('html-minifier-terser').minify;
const files = ['calculator.html', 'codepad.html', 'notepad.html', 'notes.html', 'world-clock.html', 'ruler.html'];

for (const file of files) {

  fs.readFile(
    file,
    {encoding: 'utf-8'},
    function(error, data) {

      if (!error) {

        let result = minify(
          data,
          {
            collapseWhitespace: true,
            removeComments: true,
            minifyCSS: true,
            minifyJS: true
          }
        );

        fs.writeFile('build/' + file, result, err => {
          if (error) throw error;
        });

      }

    }
  );

}
