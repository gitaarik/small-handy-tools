#!/usr/bin/env node
const fs = require('fs');
const minify = require('html-minifier-terser').minify;

if (process.argv.length != 3) {
  console.log('Please provide a file');
  process.exit();
}

const file = process.argv[2];

fs.readFile(
  file,
  {encoding: 'utf-8'},
  function(error, data) {

    if (!error) {

      let result = minify(
        data,
        {
          collapseWhitespace: true,
          removeComments: true,
          minifyCSS: true,
          minifyJS: true
        }
      );

      console.log('data:text/html,' + result.replace(/#/g, '%23'));
      // console.log('data:text/html;base64,' + Buffer.from(result).toString('base64'));

    }

  }
);
