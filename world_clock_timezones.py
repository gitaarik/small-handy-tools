import io
import json
import re
import urllib.request
import tarfile


def get_countries_data():
    datafile_url = 'https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.json'
    response = urllib.request.urlopen(datafile_url)
    return json.loads(response.read().decode())


COUNTRY_DATA = {
    data['alpha-2']: data
    for data in get_countries_data()
}


def get_timezone_data():

    datafile_url = 'https://data.iana.org/time-zones/releases/tzdata2021a.tar.gz'
    response = urllib.request.urlopen(datafile_url)

    tar = tarfile.open(fileobj=io.BytesIO(response.read()))

    file_contents = tar.extractfile('zone1970.tab').read().decode()
    tab_data = re.sub(r'^#.*\n', '', file_contents, flags=re.M).strip()

    return [
        parse_line(line)
        for line in tab_data.split('\n')
    ]


def parse_line(line):

    fields = line.split('\t')

    country_codes = fields[0].split(',')
    zone_string = fields[2]

    if len(fields) > 3:
        comments = fields[3]
    else:
        comments = ''

    return get_worldclock_data(
        zone_string,
        country_codes,
        comments
    )


def get_worldclock_data(zone_string, country_codes, comments):

    items = [zone_string]

    for country_data in get_country_data(country_codes):
        items.append(country_data['name'])

    for country_data in get_country_data(country_codes):
        items.append(country_data['region'])

    for country_data in get_country_data(country_codes):
        items.append(country_data['sub-region'])

    if comments:
        items.append(comments)

    return list(filter_empty_strings(remove_duplicates(items)))


def remove_duplicates(items):
    return dict.fromkeys(items)


def filter_empty_strings(items):
    return filter(lambda x: x, items)


def get_country_data(country_codes):
    return [
        COUNTRY_DATA[code]
        for code in country_codes
    ]


data = get_timezone_data()
print(json.dumps(data, indent=1, ensure_ascii=False))
